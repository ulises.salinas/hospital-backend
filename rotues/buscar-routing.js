// ruta  = /api/buscar

const {  Router  } = require('express');
const { check } = require('express-validator');

const { validarCampos } = require('../middlewares/validar-campos');
const { validarToken } = require("../middlewares/validar-token");
const { getBuscarTodo, getDocumentosColeccion } = require("../controllers/buscar");


const router = Router();


router.get('/:busqueda',
    [
    validarToken,
    ],
    getBuscarTodo );

router.get('/coleccion/:tabla/:busqueda',
    [
    validarToken,
    ],
    getDocumentosColeccion );



module.exports = router;
