// ruta  = /api/upload

const {  Router  } = require('express');

const expressFileUpload = require('express-fileupload');
const { check } = require('express-validator');

const { validarCampos } = require('../middlewares/validar-campos');
const { validarToken } = require("../middlewares/validar-token");
const { subirArchivo, getImagen } = require("../controllers/upload");


const router = Router();

router.use( expressFileUpload({
    responseOnLimit: 'Se ha rebasado el límite de archivos de subida'
}) );

router.put('/:tabla/:id',
    [validarToken],
    subirArchivo );

router.get('/:tabla/:archivo',
    [],
    getImagen );



module.exports = router;
