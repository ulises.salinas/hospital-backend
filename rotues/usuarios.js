const {  Router  } = require('express');
const { check } = require('express-validator');

const { getUsuarios, crearUsuario, actualizarUsuario, forzarBorrarUsuario, borrarUsuario} = require('../controllers/usuarios');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarToken } = require("../middlewares/validar-token");


const router = Router();


// la raíz de esta ruta
// ruta  = /api/usuarios

router.get('/', [validarToken] , getUsuarios );


// usar middleware para validar, varios en un array
router.post('/',
    [
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('password', 'El password es obligatorio').not().isEmpty(),
        check('email', 'El email es necesario').isEmail(),
        validarCampos, /// llamar custom-middleware
    ],
    crearUsuario
);

router.put('/:id',
    [
        validarToken,
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('email', 'El email es necesario').isEmail(),
        check('role', 'El role es obligatorio').not().isEmpty(),
        validarCampos, /// llamar custom-middleware
    ]
    , actualizarUsuario );

// soft-delete
router.delete('/:id',
    [
        validarToken
    ], forzarBorrarUsuario );



module.exports = router;
