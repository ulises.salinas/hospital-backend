// Ruta: /api/hospitales
const {  Router  } = require('express');
const { check } = require('express-validator');

const { getHospitales, crearHospital, actualizarHospital, forzarBorrarHospital, borrarHospital} = require('../controllers/hospitales');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarToken } = require("../middlewares/validar-token");


const router = Router();


// la raíz de esta ruta
// ruta  = /api/usuarios

router.get('/', [validarToken] , getHospitales );


// usar middleware para validar, varios en un array
router.post('/',
    [
        validarToken,
        check('nombre', 'El nombre del hospital es ncesario').not().isEmpty(),
        validarCampos
    ],
    crearHospital
);

router.put('/:id',
    [
        validarToken,
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        validarCampos
    ]
    , actualizarHospital );

// soft-delete
router.delete('/:id',
    [
        validarToken
    ], forzarBorrarHospital );



module.exports = router;
