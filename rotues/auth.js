const {renewToken} = require("../controllers/auth");
const {validarToken} = require("../middlewares/validar-token");
const {validarCampos} = require("../middlewares/validar-campos");
const { check  } =  require("express-validator");
const {  Router  } = require('express');


const {  login, googleSignIn  } = require('../controllers/auth');

const router = Router();

// ruta  = /api/login

router.post('/',
    [
        check('email', 'El email es obligatorio').isEmail(),
        check('password', 'El password es obligatorio').not().isEmpty(),
        validarCampos
    ],
    login);
router.post('/google',
    [
        check('token', 'El token es obligatorio').not().isEmpty(),
        validarCampos
    ],
    googleSignIn);

router.get('/renewToken',
        validarToken,
        renewToken
);



module.exports = router;
