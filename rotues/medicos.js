// Ruta: /api/hospitales
const {  Router  } = require('express');
const { check } = require('express-validator');

const { getMedicos, crearMedico, actualizarMedico, forzarBorrarMedico, borrarMedico} = require('../controllers/medicos');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarToken } = require("../middlewares/validar-token");


const router = Router();


// la raíz de esta ruta
// ruta  = /api/medicos

router.get('/', [validarToken] , getMedicos );


// usar middleware para validar, varios en un array
router.post('/',
    [
        validarToken,
        check('nombre', 'El nombre del médico es requerido').not().isEmpty(),
        check('hospital', 'El hospital del médico es requerido').not().isEmpty(),
        check('hospital', 'El hospital del médico debe ser válido').isMongoId(),
        validarCampos,
    ],
    crearMedico
);

router.put('/:id',
    [
        validarToken,
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('hospital', 'El hospital del médico es requerido').not().isEmpty(),
        check('hospital', 'El hospital del médico debe ser válido').isMongoId(),
        validarCampos
    ]
    , actualizarMedico );

// soft-delete
router.delete('/:id',
    [
        validarToken
    ], forzarBorrarMedico );



module.exports = router;
