const Usuario = require('../models/usuario');
const Hospital = require('../models/hospital');
const Medico = require('../models/medico');
const fs = require('fs');

const { logger }   = require('../helpers');

const borrarImagen = (path) => {
    if ( fs.existsSync(path)) {
        fs.unlinkSync( path );
    }

    return true;
};


const actualizarImagen = async (tipo, uuid, nombreArchivo) => {
    switch (tipo) {
        case 'hospital':
            const hospital = await Hospital.findById( uuid );

            if (!hospital) {
                logger(`El uuid no es de hospital ${ uuid } `, 'error');
                return false;
            }

            const pathHospitalViejo = `./uploads/hospital/${ hospital.img }`;
            borrarImagen(pathHospitalViejo);

            hospital.img = nombreArchivo;
            await hospital.save();
            return true;

            break;
        case 'usuario':
            const usuario = await Usuario.findById( uuid );

            if (!usuario) {
                logger(`El uuid no es de usuario ${ uuid } `, 'error');
                return false;
            }

            const pathUsuarioViejo = `./uploads/hospital/${ usuario.img }`;
            borrarImagen(pathUsuarioViejo);

            usuario.img = nombreArchivo;
            await usuario.save();
            return true;



            break;
        case 'medico':
            const medico = await Medico.findById( uuid );
            if (!medico) {
                logger(`El uuid no es de médico ${ uuid } `, 'error');
                return false;
            }

            const pathMedicoViejo = `./uploads/medico/${ medico.img }`;
            borrarImagen(pathMedicoViejo);

            medico.img = nombreArchivo;
            await medico.save();

            return true;
            break;
    }

};


module.exports = {
    actualizarImagen
};
