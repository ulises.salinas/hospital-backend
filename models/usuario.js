const { Schema, model } = require('mongoose');


const UsuarioSchema = Schema({
    nombre: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
    },
    img: {
        type: String,
    },
    role: {
        type: String,
        required: true,
        default: 'user role'
    },
    google: {
        type: Boolean,
        default: false,
    },
});

// overload params
// sobre escribir las propiedades
UsuarioSchema.method('toJSON', function() {
    // extraer las propiedades: _id y __v, dejar las demás propiedades en object
   const {_id, __v, password, ...object} =  this.toObject();
   object.uuid = _id;

   return object;
});

module.exports = model('Usuario', UsuarioSchema);
