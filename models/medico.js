const { Schema, model } = require('mongoose');


const MedicoSchema = Schema({
    nombre: {
        type: String,
        required: true,
    },
    img: {
        type: String,
    },
    usuario: {
        require: true,
        type: Schema.Types.ObjectId,
        ref: 'Usuario',

    },
    hospital: {
        require: true,
        type: Schema.Types.ObjectId,
        ref: 'Hospital',

    },
}, 
{
    collection: 'medicos'
});

// overload params
// sobre escribir las propiedades
MedicoSchema.method('toJSON', function() {
    // extraer las propiedades: _id y __v, dejar las demás propiedades en object
   const {_id, __v, ...object} =  this.toObject();
   object.uuid = _id;

   return object;
});

module.exports = model('Medico', MedicoSchema);
