const { Schema, model } = require('mongoose');


const HospitalSchema = Schema({
    nombre: {
        type: String,
        required: true,
    },
    img: {
        type: String,
    },
    usuario: {
        required: true, 
        type: Schema.Types.ObjectId,
        ref: 'Usuario',

    }
}, {
    collection: 'hospitales'
});

// overload params
// sobre escribir las propiedades
HospitalSchema.method('toJSON', function() {
    // extraer las propiedades: _id y __v, dejar las demás propiedades en object
   const {_id, __v, ...object} =  this.toObject();
   object.uuid = _id;

   return object;
});

module.exports = model('Hospital', HospitalSchema);
