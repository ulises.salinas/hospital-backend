const { response } = require('express');
const bcrypt       = require('bcryptjs');
const { v4: uuidv4 } = require('uuid');
const { actualizarImagen } = require("../helpers/actualizar-imagen");
const fs = require('fs');
const path  = require('path');

const { logger, generarJWT }   = require('../helpers');
const Usuario = require('../models/usuario');
const Hospital = require('../models/hospital');
const Medico = require('../models/medico');


const subirArchivo = async (req, res = response) => {
    const tabla = req.params.tabla;
    const uuid = req.params.id;

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            msg: 'Es necesario el archivo imagen',
        });
    }
    const imagen = req.files.imagen;
    const fileName = imagen.name;
    const nombreCortado = fileName.split('.');
    const extensionArchivo = nombreCortado[ nombreCortado.length -1  ];


    const extensionesValidas = ['jpg','png','jpeg','gif'];
    if (! extensionesValidas.includes( extensionArchivo )) {
        return res.status(404).json({
            ok : false,
            msg: `Archivo no permitido. Sólo: ${extensionesValidas.join(', ')}`,
        });
    }

    const tiposValidos = ['hospital', 'usuario', 'medico'];
    if (! tiposValidos.includes( tabla )) {
        return res.status(404).json({
            ok : false,
            msg: `El tipo no es válido, solo se permiten: ${tiposValidos.join(', ') }`,
        });
    }

    // Generar nombre del archivo
    const nombreArchivo = `${ uuidv4() }.${ extensionArchivo }`;
    // ruta para guardar la imagen
    const path = `./uploads/${ tabla }/${ nombreArchivo }`;


    // Use the mv() method to place the file somewhere on your server
    // mover la imagen
    imagen.mv(path, (err) => {
        if (err) {
            logger(err, 'error');
            return res.status(500).json({
                ok: false,
                msg: 'Error al mover la imagen'
            });
        }


        // actualizar base de datos
        actualizarImagen(tabla, uuid, nombreArchivo);


        return res.json({
            ok: true,
            msg: 'File uploaded!',
            nombreArchivo
        });
    });


};

const getImagen = async ( req, res = response) => {
    const tabla = req.params.tabla;
    const archivo = req.params.archivo;

    const pathImg = path.join(__dirname, `../uploads/${ tabla }/${ archivo }`);

    /// imagen por defecto
    if ( ! fs.existsSync(pathImg)  ) {
        res.sendFile( path.join(__dirname, `../uploads/no-img.jpg`) );
    } else {
        res.sendFile( pathImg );
    }
};

module.exports = {
    subirArchivo, getImagen
};
