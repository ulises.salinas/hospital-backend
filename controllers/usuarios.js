const { response } = require('express');
const bcrypt       = require('bcryptjs');


const { logger, generarJWT }   = require('../helpers');
const Usuario = require('../models/usuario');

const getUsuarios = async (req, res) => {


    const desde = Number(req.query.desde )  || 0;
    const cantidad = Number(req.query.cantidad ) || 5;

    // devolver todos los usuarios, devolver las columnas nombre, email role, google...
    /* const usuarios = await Usuario
                .find({}, 'nombre email role google img')
                .skip(desde)
                .limit(cantidad); */

    // total de registros

    /* const totalRegistros = await Usuario.count() */


    // disparar los dos métodos de forma simultanea, traeme la cuenta de todos, traeme los datos paginados
    // devuelve un array con el resultado de las promesas,
    // desestructuramos para obtener en 'usuarios', el resultado de la primer promesa y en totalRegistros el
    // resultado de la segunda promresa
    // mas eficiente  
    const [usuarios, totalRegistros] = await Promise.all([
        Usuario.find({}, 'nombre email role google img')
               .skip(desde)
               .limit(cantidad),
        Usuario.countDocuments()

    ]);

    res.json({
        ok : true,
        msg: 'getUsuario',
        data: usuarios,
        totalRegistros,
        paginado: {
            desde,
            cantidadPorHoja: cantidad
        }
        /*
        // viene del middleware validar-token
        usuario_auth: {
            nombre: req.nombre,
            email: req.nombre,
            uuid: req.uuid,
        },*/
    });
};

/**
 * Crear un usuario, validar los campos usando un middleware en la ruta
 * @see validar-campos.js
 * @param req
 * @param res
 * @returns {Promise<any>}
 */
const crearUsuario =  async (req, res = response) => {
    // extraer las columnas o propiedades
    const { password, email } = req.body;

    /*// hubo errores??
    const errores = validationResult( req );
    if ( ! errores.isEmpty() ) {
        return res.status(403).json({
            ok: false,
            errors: errores.mapped(),
            msg: "Errores"
        });
    }*/


    try {

        // espera a que termine la búsqueda
        const existeUsuario = await Usuario.findOne( { email });

        if ( existeUsuario ) {
            return res.status(403).json({
                ok: false,
                msg: "El correo no puede duplicarse"
            });
        }
        const usuario = new Usuario( req.body );

        // Encriptar contraseña
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync( password, salt );

        // espera a que termine... esto es el await y la función principal debe ser async
        await usuario.save();
        const token = await generarJWT(usuario);
        await res.json({
            ok: true,
            msg: 'crear usuario',
            data: usuario,
            token
        });

    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};


const actualizarUsuario = async (req, res = response) => {
    const uuid = req.params.id;
    // TODO: validar token de validación


    try {

        const usuario = await Usuario.findById( uuid );

        if (!usuario) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el usuario"
            });
        }

        // Actualizar
        // borrar los campos que no quiero cambiar
        const {password, google, email, ...campos} = req.body;

        if (usuario.email !== email) {
            const existeUsuario = await Usuario.findOne( {email});
            if ( existeUsuario ) {
                return res.status(403).json({
                    ok: false,
                    msg: "El correo no puede duplicarse"
                });
            }
        }
        
        if( ! usuario.google ) {
            campos.email = email;
        }
        else if ( usuario.email !== email ){
            return res.status(403).json({
                ok: false,
                msg: "El usuario no puede cabiar su correo"
            });
        }
        

        const usuarioActualizado = await Usuario.findByIdAndUpdate(uuid, campos, {new : true});



        await res.json({
            ok: true,
            msg: 'actualizar usuario',
            data: usuarioActualizado
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

const forzarBorrarUsuario = async (req, res = response) => {
    const uuid = req.params.id;

    try {

        const usuario = await Usuario.findById( uuid );

        if (!usuario) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el usuario"
            });
        }


        const usuarioActualizado = await Usuario.findByIdAndDelete( uuid );



        await res.json({
            ok: true,
            msg: 'Usuario eliminado',
            data: usuarioActualizado
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

const borrarUsuario = async (req, res = response) => {
    const uuid = req.params.id;

    try {

        const usuario = await Usuario.findById( uuid );

        if (!usuario) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el usuario"
            });
        }

        // Actualizar
        // borrar los campos que no quiero cambiar
        const campos = { activo: false };

        const usuarioActualizado = await Usuario.findByIdAndUpdate(uuid, campos, {new : true});



        await res.json({
            ok: true,
            msg: 'actualizar usuario',
            data: usuarioActualizado
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

module.exports = {
    getUsuarios,
    crearUsuario,
    actualizarUsuario,
    forzarBorrarUsuario,
    borrarUsuario,
};
