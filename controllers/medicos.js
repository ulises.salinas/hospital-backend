const { response } = require('express');
const bcrypt       = require('bcryptjs');


const { logger }   = require('../helpers');
const Medico = require('../models/medico');
const Hospital = require('../models/hospital');

const getMedicos = async (req, res = response) => {
    // devolver todos los hospitales, devolver las columnas nombre, email role, google...
    const medicos = await Medico.find()
                                .populate('usuario','nombre img')
                                .populate('hospital','nombre img');

    res.json({
        ok : true,
        msg: 'getMedicos',
        data: medicos,
       
    });
};

/**
 * Crear un usuario, validar los campos usando un middleware en la ruta
 * @see validar-campos.js
 * @param req
 * @param res
 * @returns {Promise<>}
 */
const crearMedico =  async (req, res = response) => {
    /// extraer las columnas o propiedades
    // const { password, email } = req.body;
    const uuidUsuario = req.uuid; /// @see middleware: validar-token
    const {hospital,...datosMedico} = req.body;

    try {

        const hospitalDB = await Hospital.findById( hospital );


        if (!hospitalDB) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el hospital"
            });
        }

        const medico = new Medico( {
            usuario: uuidUsuario,
            hospital,
            ...datosMedico,
        } );

      
        // espera a que termine... esto es el await y la función principal debe ser async
        const registro = await medico.save();


        res.json({
            ok: true,
            msg: 'crear médico',
            data: registro,
            
        });

    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};


const actualizarMedico = async (req, res = response) => {
    const uuid = req.params.id;
    const uuidUsuario = req.uuid;

    try {

        const medico = await Medico.findById( uuid );

        if (!medico) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró al médico"
            });
        }

        // Actualizar
        // borrar los campos que no quiero cambiar
        const cambiosMedico = {
            ...req.body,
            usuario: uuidUsuario
        };


        const medicoActualizado = await Medico.findByIdAndUpdate(uuid, cambiosMedico, {new : true});



        res.json({
            ok: true,
            msg: 'actualizar médico',
            data: medicoActualizado
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

const forzarBorrarMedico = async (req, res = response) => {
    const uuid = req.params.id;

    try {

        const medico = await Medico.findById( uuid );

        if (!medico) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró al médico"
            });
        }


        await Medico.findByIdAndDelete( uuid );



        res.json({
            ok: true,
            msg: 'Médico eliminado',
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

const borrarMedico = async (req, res = response) => {
    const uuid = req.params.id;

    try {

        const medico = await Medico.findById( uuid );

        if (!medico) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró al médico"
            });
        }

        // Actualizar
        // borrar los campos que no quiero cambiar
        const campos = { activo: false };

        const medicoActualizado = await Medico.findByIdAndUpdate(uuid, campos, {new : true});



        res.json({
            ok: true,
            msg: 'actualizar médico',
            data: medicoActualizado
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

module.exports = {
    getMedicos,
    crearMedico,
    actualizarMedico,
    forzarBorrarMedico,
    borrarMedico,
};
