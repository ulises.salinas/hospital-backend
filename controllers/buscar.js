const { response } = require('express');
const bcrypt       = require('bcryptjs');


const { logger, generarJWT }   = require('../helpers');
const Usuario = require('../models/usuario');
const Hospital = require('../models/hospital');
const Medico = require('../models/medico');


const getBuscarTodo = async (req, res = response) => {
    const desde = Number(req.query.desde )  || 0;
    const cantidad = Number(req.query.cantidad ) || 5;
    let totalRegistros = 0;
    const busqueda = req.params.busqueda;
    const regxep = new RegExp( busqueda, 'i'  );




    const [usuarios,medicos, hospitales ] = await Promise.all([
    Usuario.find({ nombre: regxep }),
    Medico.find({ nombre: regxep }),
    Hospital.find({ nombre: regxep }),
    ]);

    res.json({
        ok : true,
        msg: 'buscarTodo',
        data: {
            usuarios,
            medicos,
            hospitales,
        },
        totalRegistros,
        paginado: {
            desde,
            cantidad
        },
        busqueda
        /*
        // viene del middleware validar-token
        usuario_auth: {
            nombre: req.nombre,
            email: req.nombre,
            uuid: req.uuid,
        },*/
    });
};



const getDocumentosColeccion = async (req, res = resposne) => {

    try {
        const desde = Number(req.query.desde )  || 0;
        const cantidad = Number(req.query.cantidad ) || 5;
        const paraBuscar = req.params.busqueda;
        const nombreTabla = req.params.tabla;
        const regExp = new RegExp( paraBuscar, 'i'  );
        let totalRegistros = 0;
        let resultado = [];


        switch ( nombreTabla ) {
            case 'usuarios':
                [resultado, totalRegistros] = await  Promise.all([
                    Usuario.find({ nombre: regExp }).skip(desde).limit(cantidad),
                    Usuario.countDocuments(),
                ]);
                break;
            case 'medicos':
                [resultado, totalRegistros] = await Promise.all([
                    Medico.find({ nombre: regExp }).populate('usuario', 'nombre img').populate('hospital', 'nombre img').skip(desde).limit(cantidad),
                    Medico.countDocuments(),
                ]);
                break;
            case 'hospitales':
                [resultado, totalRegistros] = await Promise.all([
                    Hospital.find({ nombre: regExp }).populate('usuario', 'nombre img').skip(desde).limit(cantidad),
                    Hospital.countDocuments(),
                ]);
                break;
            default:
               return res.status(404).json({
                    ok: false,
                    msg: "Falta definir una colección válida"
                });
        }

        res.json({
            ok : true,
            msg: 'buscarColeccion',
            data: resultado,
            totalRegistros,
            paginado: {
                desde,
                cantidad
            },
            busqueda: paraBuscar,
            tabla: nombreTabla,

        });

    } catch (e) {
        logger('Error inesperado', 'error');
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error no definido"
        });
    }

};



module.exports = {
    getBuscarTodo,
    getDocumentosColeccion,
};
