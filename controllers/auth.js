const { response } = require('express');
const bcrypt       = require('bcryptjs');

const Usuario = require('../models/usuario');
const { logger, generarJWT }   = require('../helpers');
const { google_verify } = require("../helpers/google-verfy");



const login = async (req, res = response) => {

    const { email, password } = req.body;

    try {

        // verificar email
        const existeUsuario = await Usuario.findOne( { email });

        if ( ! existeUsuario ) {
            return res.status(403).json({
                ok: false,
                msg: "Credenciales incorrectas"
            });
        }

        // verificar contraseña
        const validPassword = bcrypt.compareSync( password, existeUsuario.password );
        if ( ! validPassword ) {
            return res.status(403).json({
                ok: false,
                msg: "Credenciales incorrectas"
            });
        }

        // generar un token JWT

        const token = await generarJWT(existeUsuario);

        await res.json({
            ok: true,
            msg: 'Login usuario',
            token
        });

    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }



};

const googleSignIn = async (req, res = response) => {
    const  googleToken  = req.body.token;
    try {
        const {name, email, picture} = await google_verify( googleToken );
        // const
        const usuarioDB = await Usuario.findOne({ email });
        let usuario ;
        if ( ! usuarioDB ) {
            usuario = new Usuario({
                nombre: name,
                email,
                password: '@@@@-google-sign-in-@@@@@@',
                img: picture,
                google: true,
            });
        }
        else {
            usuario = usuarioDB;
            usuario.google = true;
           //   usuario.password = '@@@@-google-sign-in-@@@@@@'; /// pierde autenticación normañ
        }

        await  usuario.save();
        const token = await generarJWT( usuario );


        res.json({
            ok: true,
            msg: 'google ok',
            token
        });
    } catch (e) {
        logger(e, 'error');
        res.status(401).json({
            ok: false,
            msg: 'Token incorrecto',
            googleToken
        });
    }


};

/**
 * regresar un token, se requiere un token viejo y generar un nuevo token
 * se va a actualizar un token que ya está en uso
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const renewToken = async (req, res = response) => {
    // el uuid viene después de que entra al middleware validar-token
    const  uuid  = req.uuid;
    // ya tenemos el uuid
    const usuario = await Usuario.findById( uuid );
    const token = await generarJWT( usuario );

    res.json({
        ok: true,
        token,
        usuario
    });

};

module.exports = {
    login, googleSignIn, renewToken
};
