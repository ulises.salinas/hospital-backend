const { response } = require('express');
const bcrypt       = require('bcryptjs');


const { logger }   = require('../helpers');
const Hospital = require('../models/hospital');

const getHospitales = async (req, res = response) => {
    // devolver todos los hospitales, devolver las columnas nombre, email role, google...
    const hospitales = await Hospital.find()
                                     .populate('usuario','nombre img');

    res.json({
        ok : true,
        msg: 'getHospitales',
        data: hospitales,
       
    });
};

/**
 * Crear un usuario, validar los campos usando un middleware en la ruta
 * @see validar-campos.js
 * @param req
 * @param res
 * @returns {Promise<any>}
 */
const crearHospital =  async (req, res = response) => {
    /// extraer las columnas o propiedades
    // const { password, email } = req.body;
    const uuidUsuario = req.uuid; /// @see middleware: validar-token

    try {

        const hospital = new Hospital( {
            usuario: uuidUsuario,
            ...req.body,
        } );
        
        
        // espera a que termine... esto es el await y la función principal debe ser async
       const registro =  await hospital.save();


        res.json({
            ok: true,
            msg: 'crear hospital',
            data: registro,
            
        });

    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};


const actualizarHospital = async (req, res = response) => {
    const uuid = req.params.id;
    const uuidUsuario = req.uuid;

    try {
        const hospital = await Hospital.findById( uuid );
        if (!hospital) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el hospital"
            });
        }
        // Actualizar
        // borrar los campos que no quiero cambiar
        const cambiosHospital = {
            ...req.body,
            usuario: uuidUsuario
        };
        const hospitalActualizado = await Hospital.findByIdAndUpdate(uuid, cambiosHospital, {new : true});
        res.json({
            ok: true,
            msg: 'actualizar hospital',
            data: hospitalActualizado
        });
    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

const forzarBorrarHospital = async (req, res = response) => {
    const uuid = req.params.id;
    try {
        const hospital = await Hospital.findById( uuid );
        if (!hospital) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el hospital"
            });
        }

        await Hospital.findByIdAndDelete( uuid );
        res.json({
            ok: true,
            msg: 'Hospital eliminado'
        });
    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

const borrarHospital = async (req, res = response) => {
    const uuid = req.params.id;

    try {

        const hospital = await Hospital.findById( uuid );

        if (!hospital) {
            return res.status(403).json({
                ok: false,
                msg: "No se encontró el usuario"
            });
        }

        // Actualizar
        // borrar los campos que no quiero cambiar
        const campos = { activo: false };

        const hospitalActualizado = await Hospital.findByIdAndUpdate(uuid, campos, {new : true});



        res.json({
            ok: true,
            msg: 'actualizar usuario',
            data: hospitalActualizado
        });



    } catch(e) {
        logger(e, 'error');
        res.status(500).json({
            ok: false,
            msg: "Error inesperado"
        });
    }
};

module.exports = {
    getHospitales,
    crearHospital,
    actualizarHospital,
    forzarBorrarHospital,
    borrarHospital,
};
