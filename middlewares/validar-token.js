const {logger} = require("../helpers");
const jwt = require('jsonwebtoken');

const { response, request } = require('express');

const validarToken = (request=request, response = response, next) => {
    // leer el token desde los headers
    const token = request.header('x-token');

    if (!token) {
        return response.status(401).json({
            ok: false,
            msg: 'No autorizado'
        })
    }


    try {

        // validar token
        const { uuid, nombre, email  } = jwt.verify( token, process.env.JWT_SALT);
        request.uuid = uuid;
        request.nombre = nombre;
        request.email = email;
        // usuario autenticado
        next();

    } catch(err) {
        logger(err, 'error');
        return response.status(401).json({
            ok: false,
            msg: 'Token incorrecto',

        })
    }
};


module.exports = {
    validarToken
};
