const { response } = require('express');
const { validationResult } = require('express-validator');


const validarCampos = (request, response = response, next) => {

    // hubo errores??
    const errores = validationResult( request );
    if ( ! errores.isEmpty() ) {
        return response.status(403).json({
            ok: false,
            errors: errores.mapped(),
            msg: "Errores"
        });
    }

    next();

};

module.exports = {
    validarCampos
};
