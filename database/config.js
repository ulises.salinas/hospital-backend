const mongoose = require('mongoose');
const { logger } = require('../helpers');
const dbConnection = async () => {

    const strConnection = `${process.env.DB_CONNECTION_STR
        .replace('<USER>', process.env.DB_USER)
        .replace('<PASSWORD>', process.env.DB_PASSWORD)
        .replace('<DATABASE_NAME>', process.env.DB_DATABASE_NAME)}`;
    logger('connection string: ' + strConnection);
    try {
        await mongoose.connect(strConnection, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });
        logger("Conectado a ATLAS");
    } catch (e) {
        throw new Error(`Error al inicializar la base de datos`)
    }
};
module.exports = {
    dbConnection
};
