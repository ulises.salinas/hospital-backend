const jwt = require('jsonwebtoken');

const colores = {
    green:  '\x1B[32m',
    red:    '\x1B[31m',
    black:  '\x1B[39m',
    yellow: '\x1B[33m',
};

const logger = (msg, type = 'log') => {
    if (process.env.APP_MODE === 'prod' ) {
        type = '';
    }
    switch (type) {
        case "error":
            console.error(`${colores.red}ERROR - [ ${ (new Date()).toISOString() } ] - ${colores.black}`, msg);
            break;
        case "debug":
            console.log(`${colores.yellow}DEBUG - [ ${ (new Date()).toISOString() } ] - ${colores.black}`, msg);
            break;
        case "log":
            console.log(`${colores.green}LOG - [ ${ (new Date()).toISOString() } ] - ${colores.black}`, msg);
    }
};

const toObjectId = (string) => {
    const ObjectId = require('mongoose').Types.ObjectId;
    return new ObjectId(string);
};

const generarJWT = (usuario) => {

    return new Promise( (resolve, reject) => {

        const payload  = {
            uuid: usuario.id,
            nombre: usuario.nombre,
            email: usuario.email,
            generatedAt: (new Date()).toISOString()
        };

        jwt.sign( payload, process.env.JWT_SALT , {
            expiresIn: '12h'
        } , (err, token) => {
            if (err) {
                logger(err, 'error');
                reject('No se pudo generar token')
            }
            else {
                resolve(token);
            }



        } );
    });

};

module.exports = {
    logger,
    toObjectId,
    generarJWT
};

