require('dotenv').config();

const express = require('express');
const cors = require('cors');

const { logger } = require('./helpers');
const { dbConnection }  = require('./database/config');

const puertoEscucha = process.env.SRV_PORT;

// aplicación express
const app = express();

//  CORS
app.use(cors());

// lectura y parseo del body
app.use(express.json());


// base de datos
dbConnection();

// directorio p{ublico
app.use(express.static('public'));

/*
* database polices
* user: mean_user
* pass: C0IYmbLdge9PuOny
* */
// Rutas

// rutas agrupadas en /api/usuarios
// cuando se haga una petición a /api/usuarios... pasar la petición al router de usuarios
app.use('/api/usuarios', require('./rotues/usuarios'));
app.use('/api/hospitales', require('./rotues/hospitales'));
app.use('/api/medicos', require('./rotues/medicos'));


app.use('/api/login', require('./rotues/auth'));
app.use('/api/buscar', require('./rotues/buscar-routing'));


app.use('/api/upload', require('./rotues/upload-routing'));


/*
app.get('/', (request, response) => {
    // el status se puede omitir si es code 200
  response.status(200).json(  {
      ok : true,
      msg: 'done'
  });
});

app.get('/api/usuarios', (req, res) => {
    res.json({
        ok : true,
        data: [
            {
                uno: 1
            }
        ]
    });
});
*/

app.listen( puertoEscucha, () => {
    logger( `Escuchado en el puerto ${puertoEscucha}`)
} );
